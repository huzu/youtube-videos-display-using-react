## Brief About This Project

- We have built a UI where we display a youtube video same as we see on youtube.com.
- To achieve this we have used youtube's data api and have fetched videos as per the         search term entered by user.
- We will have a video list, video item and video detail components for video                respectively. 

## Important Points To Remember

- In this project we have created an axios instance with default values to call the          YouTube API with. The axios library released v0.19.0 which seems to have included a bug    that was never resolved in the beta version. This will cause a 400 error failure with      the message "Required Parameter : part". The get around this and follow along with the     course, do the following:
  `npm uninstall axios`
  
  `npm install axios@0.18.0`

- The issue appears to only revolve around the params object so the other projects we have   created or will create in the future will not be affected.
