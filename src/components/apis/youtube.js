import axios from 'axios';

const KEY = "AIzaSyCisImNacqfJN7ODA3S-30q2jy-SXizTwY";

export default axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3",
  params: {
    part: 'snippet',
    maxResults: 5,
    key: KEY
  }
});